function doGet() {
  var html = HtmlService.createTemplateFromFile("Map").evaluate();
  html.setTitle("D&D Map");
  return html;
}

function include(filename) {
  return HtmlService.createHtmlOutputFromFile(filename)
      .setSandboxMode(HtmlService.SandboxMode.IFRAME)
      .getContent();
}
